# Modulo de terraform para crear buckets en s3

Este modulo esta basado en el modulo oficial de terraform para la creacion de buckets en s3:
https://registry.terraform.io/modules/terraform-aws-modules/s3-bucket/aws/1.17.0 


### Inputs

| Nombre  | Descripción  | Requerido | Default  |
|---|---|---|---|
| bucket_name  | Nombre del bucket  | SI  | N/A  |
| bucket_acl  | ACL del bucket  | NO  | private  |
| bucket_versioning  | Habilitar versionado del bucket  | NO  | false  |
| bucket_website_config  | Configuracion de hosting de paginas web  | NO  | {}  |
| bucket_logging_config  | Configuracion de logs del bucket  | NO  | {}  |
| bucket_block_public_acls  | Configuracion global de ACL  | NO  | true  |
| bucket_block_public_policy  | Configuracion global de policies publicos  | NO  | true  |
| bucket_ignore_public_acls  | Configuracion global de ignorar policies publicos  | NO  | true  |
| bucket_restrict_public_buckets  | Configuracion global de restringir policies publicos  | NO  | true  |
| attach_elb_log_delivery_policy  | Configuracion policies para logs  | NO  | false  |
| tags  | Mapa de tags para asignar a   | NO  | null  |


### Outputs

| Nombre  | Descripción  |
|---|---|
| this_s3_bucket_id  | Id del bucket creado  |
| this_s3_bucket_bucket_regional_domain_name  | Region especifica del bucket en nombre de dominio  |
| this_s3_bucket_arn  | Arn del bucket creado  |
| this_s3_bucket_bucket_domain_name  | Nombre de dominio del bucket creado  |
| this_s3_bucket_hosted_zone_id  | Hostedzone de route 53 del bucket creado  |
| this_s3_bucket_region  | Region del bucket creado  |
| this_s3_bucket_website_domain  | Dominio del enpoint del website del bucket creado. Solo si el bucket fue creado como website. Usado para crear alias en route 53  |
| this_s3_bucket_website_endpoint  | Enpoint del website del bucket creado. Solo si el bucket fue creado como website.  |
