output "this_s3_bucket_id" {
    value = module.s3_bucket.this_s3_bucket_id
}

output "this_s3_bucket_bucket_regional_domain_name"  {
    value = module.s3_bucket.this_s3_bucket_bucket_regional_domain_name
}

output "this_s3_bucket_arn"  {
    value = module.s3_bucket.this_s3_bucket_arn
}

output "this_s3_bucket_bucket_domain_name"  {
    value = module.s3_bucket.this_s3_bucket_bucket_domain_name
}

output "this_s3_bucket_hosted_zone_id"  {
    value = module.s3_bucket.this_s3_bucket_hosted_zone_id
}

output "this_s3_bucket_region"  {
    value = module.s3_bucket.this_s3_bucket_region
}

output "this_s3_bucket_website_domain"  {
    value = module.s3_bucket.this_s3_bucket_website_domain
}

output "this_s3_bucket_website_endpoint"  {
    value = module.s3_bucket.this_s3_bucket_website_endpoint
}