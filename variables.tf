# REQUIRED PARAMETERS

variable bucket_name {
  type        = string
  description = "Nombre del bucket"
}


# OPTIONAL PARAMETERS


variable bucket_acl {
  type        = string
  default     = "private"
  description = "ACL del bucket"
}

variable bucket_versioning {
  type        = bool
  default     = false
  description = "Habilitar versionado del bucket"
}

variable bucket_website_config {
  type        = map(any)
  default     = {}
  description = "Configuracion de hosting de paginas web"
}

variable bucket_logging_config {
  type        = map(any)
  default     = {}
  description = "Configuracion de logs del bucket"
}

variable bucket_block_public_acls {
  type        = bool
  default     = true
  description = "Configuracion global de ACL"
}

variable bucket_block_public_policy {
  type        = bool
  default     = true
  description = "Configuracion global de policies publicos"
}

variable bucket_ignore_public_acls {
  type        = bool
  default     = true
  description = "Configuracion global de ignorar policies publicos"
}

variable bucket_restrict_public_buckets {
  type        = bool
  default     = true
  description = "Configuracion global de restringir policies publicos"
}

variable attach_elb_log_delivery_policy {
  type        = bool
  default     = false
  description = "Configuracion policies para logs"
}

variable tags {
  type        = map(string)
  description = "Tags asociados al bucket"
  default     = {}
}