module "s3_bucket" {
  source  = "terraform-aws-modules/s3-bucket/aws"
  version = "1.16.0"

  bucket = var.bucket_name
  acl    = var.bucket_acl

  versioning = {
    enabled = var.bucket_versioning
  }

  website = var.bucket_website_config

  logging                        = var.bucket_logging_config
  attach_elb_log_delivery_policy = var.attach_elb_log_delivery_policy


  # S3 bucket-level Public Access Block configuration
  block_public_acls       = var.bucket_block_public_acls
  block_public_policy     = var.bucket_block_public_policy
  ignore_public_acls      = var.bucket_ignore_public_acls
  restrict_public_buckets = var.bucket_restrict_public_buckets
  tags                    = var.tags
}